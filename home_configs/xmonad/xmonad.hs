
  -- Base
import XMonad
import System.Directory
import System.IO (hPutStrLn)
import System.Exit (exitSuccess)
import qualified XMonad.StackSet as W

    -- Actions
import XMonad.Actions.CopyWindow (kill1)
import XMonad.Actions.CycleWS (Direction1D(..), moveTo, shiftTo, WSType(..), nextScreen, prevScreen, shiftNextScreen, shiftPrevScreen, nextWS, prevWS, shiftToNext, shiftToPrev)
import XMonad.Actions.GridSelect
import XMonad.Actions.MouseResize
import XMonad.Actions.Promote
import XMonad.Actions.RotSlaves (rotSlavesDown, rotAllDown)
import XMonad.Actions.WindowGo (runOrRaise)
import XMonad.Actions.WithAll (sinkAll, killAll)
import qualified XMonad.Actions.Search as S

    -- Data
import Data.Char (isSpace, toUpper)
import Data.Maybe (fromJust)
import Data.Monoid
import Data.Maybe (isJust)
import Data.Tree
import qualified Data.Map as M

    -- Hooks
import XMonad.Hooks.DynamicLog (dynamicLogWithPP, wrap, xmobarPP, xmobarColor, shorten, PP(..))
import XMonad.Hooks.EwmhDesktops  -- for some fullscreen events, also for xcomposite in obs.
import XMonad.Hooks.ManageDocks (avoidStruts, docksEventHook, manageDocks, ToggleStruts(..))
import XMonad.Hooks.ManageHelpers (isFullscreen, doFullFloat)
import XMonad.Hooks.ServerMode
import XMonad.Hooks.SetWMName
import XMonad.Hooks.WorkspaceHistory

    -- Layouts
import XMonad.Layout.Accordion
import XMonad.Layout.GridVariants (Grid(Grid))
import XMonad.Layout.SimplestFloat
import XMonad.Layout.Spiral
import XMonad.Layout.ResizableTile
import XMonad.Layout.Tabbed
import XMonad.Layout.ThreeColumns

    -- Layouts modifiers
import XMonad.Layout.LayoutModifier
import XMonad.Layout.LimitWindows (limitWindows, increaseLimit, decreaseLimit)
import XMonad.Layout.Magnifier
import XMonad.Layout.MultiToggle (mkToggle, single, EOT(EOT), (??))
import XMonad.Layout.MultiToggle.Instances (StdTransformers(NBFULL, MIRROR, NOBORDERS))
import XMonad.Layout.NoBorders
import XMonad.Layout.Renamed
import XMonad.Layout.ShowWName
import XMonad.Layout.Simplest
import XMonad.Layout.Spacing
import XMonad.Layout.SubLayouts
import XMonad.Layout.WindowArranger (windowArrange, WindowArrangerMsg(..))
import qualified XMonad.Layout.ToggleLayouts as T (toggleLayouts, ToggleLayout(Toggle))
import qualified XMonad.Layout.MultiToggle as MT (Toggle(..))

   -- Utilities
import XMonad.Util.Dmenu
import XMonad.Util.EZConfig (additionalKeysP)
import XMonad.Util.NamedScratchpad
import XMonad.Util.Run (runProcessWithInput, safeSpawn, spawnPipe)
import XMonad.Util.SpawnOnce


-- constance

myFont :: String
myFont = "xft:Monoki Nerd Font:regular:size=9:antialias=true:hinting=true,FontAwesome:pixelsize=9"

myModMask :: KeyMask
myModMask = mod4Mask        -- Sets modkey to super/windows key

myTerminal :: String
myTerminal = "alacritty"    -- Sets default terminal

myBrowser :: String
myBrowser = "firefox "  -- Sets qutebrowser as browser

myEmacs :: String
myEmacs = "emacsclient -c -a 'emacs' "  -- Makes emacs keybindings easier to type

myEditor :: String
myEditor = "emacsclient -c -a 'emacs' "  -- Sets emacs as editor
-- myEditor = myTerminal ++ " -e vim "    -- Sets vim as editor

myBorderWidth :: Dimension
myBorderWidth = 1           -- Sets border width for windows

windowCount :: X (Maybe String)
windowCount = gets $ Just . show . length . W.integrate' . W.stack . W.workspace . W.current . windowset

-- Colors for theaming

myBgColor :: String
myBgColor = "#222d32"

myInactiveBgColor :: String
myInactiveBgColor = "#2f434c"

myTextColor :: String
myTextColor = "#f3f4f5"   

myInactiveTextColor :: String
--myInactiveTextColor = "#676E7D"
myInactiveTextColor = "#bbc1cc"

myActiveTextColor :: String
myActiveTextColor = "#010101"

myActiveBgColor :: String
myActiveBgColor = "#16a085"

myUrgentBgColor :: String
myUrgentBgColor = "#E53935"

myBorderColor :: String
myBorderColor = "#222d32"

-- setup
myStartupHook :: X ()
myStartupHook = do
    spawnOnce "lxsession &"
    spawnOnce "picom &"
  --  spawnOnce "pulseeffects --gapplication-service &"
    spawnOnce "nm-applet &"
    spawnOnce "volumeicon &"
    spawnOnce "birdtray &"
    spawnOnce "nextcloud --background &"
    spawnOnce "/usr/lib/notification-daemon-1.0/notification-daemon &"
    spawnOnce "pgrep redshift || redshift -l 50.65205:9.16344 -t 3500:3000 &"
    spawnOnce "alacritty -e $HOME/.config/scripts/updateScript.sh"
    spawnOnce "xsetroot -cursor_name left_ptr" -- Fix black-cross cursor issue
    spawnOnce "/home/pc/ArduinoCreateAgent/Arduino_Create_Agent"
    --spawnOnce "conky -c $HOME/.config/conky/xmonad.conkyrc"
    spawnOnce "trayer --edge top --align right --widthtype request --padding 6 --SetDockType true --SetPartialStrut true --expand true --monitor 1 --transparent true --alpha 0 --tint 0x282c34  --height 22 &"
    --spawnOnce "/usr/bin/emacs --daemon &" -- emacs daemon for the emacsclient
    spawnOnce "/usr/bin/srandrd /home/pc/.config/scripts/hotplug.sh"
    spawnOnce "/usr/bin/gio trash /home/pc/tmp/*"
    -- spawnOnce "kak -d -s mysession &"  -- kakoune daemon for better performance
    -- spawnOnce "urxvtd -q -o -f &"      -- urxvt daemon for better performance

    --spawnOnce "xargs xwallpaper --stretch < ~/.xwallpaper"  -- set last saved with xwallpaper
    -- spawnOnce "/bin/ls ~/wallpapers | shuf -n 1 | xargs xwallpaper --stretch"  -- set random xwallpaper
    -- spawnOnce "~/.fehbg &"  -- set last saved feh wallpaper
    -- spawnOnce "feh --randomize --bg-fill ~/wallpapers/*"  -- feh set random wallpaper
    spawnOnce "feh --bg-fill ~/Pictures/wallpaper/wallpaper.jpg"
    -- spawnOnce "nitrogen --restore &"   -- if you prefer nitrogen to feh
    setWMName "LG3D"

-- Workspaces
myWorkspaces = [" 1: \xf120 ", " 2: \xf269 ", " 3: \xf20e ", " 4: \xf20e ", " 5: \xf20e ", " 6: \xf20e ", " 7: \xf20e ", " 8: \xf20e ", " 9: \xf003 "]
myWorkspaceIndices = M.fromList $ zipWith (,) myWorkspaces [1..9] -- (,) == \x y -> (x,y)

clickable ws = "<action=xdotool key super+"++show i++">"++ws++"</action>"
    where i = fromJust $ M.lookup ws myWorkspaceIndices

shiftAndMove w = W.view w . W.shift w

myKeys :: [(String, X ())]
myKeys =
  [ ("M-C-r", spawn "xmonad --recompile")
  , ("M-S-r", spawn "xmonad --restart")
  , ("M-S-e", io exitSuccess)              -- Quits xmonad
  , ("M-C-p", spawn "poweroff")
  , ("M-S-p", spawn "reboot")

  , ("M-r", spawn "rofi -show drun -config ~/.config/rofi/config.rasi -display-drun \"Run: \" -drun-display-format \"{name}\"") -- Rofi

  , ("M-x", spawn (myTerminal ++ " -e fish"))
  , ("M-b", spawn (myBrowser))
  , ("M-S-b", spawn (myBrowser ++ " --private-window"))
  , ("M-e", spawn "pcmanfm" )
  , ("M-n", spawn "xournalpp" )

  , ("M-q", kill1)     -- Kill the currently focused client
  , ("M-S-q", killAll)   -- Kill all windows on current workspace

-- Auto-Theaminx
  , ("M-S-t", spawn "~/.config/scripts/changeTheme.sh 1")
  , ("M-C-t", spawn "~/.config/scripts/changeTheme.sh 0")

-- Floating windows
  , ("M-f", sendMessage (T.Toggle "floats")) -- Toggles my 'floats' layout
  , ("M-t", withFocused $ windows . W.sink)  -- Push floating window back to tile

-- Window resizing
  , ("M-h", sendMessage Shrink)                   -- Shrink horiz window width
  , ("M-l", sendMessage Expand)                   -- Expand horiz window width
  , ("M-M1-j", sendMessage MirrorShrink)          -- Shrink vert window width
  , ("M-M1-k", sendMessage MirrorExpand)          -- Expand vert window width


-- Windows navigation
  , ("M-m", windows W.focusMaster)  -- Move focus to the master window
  , ("M-j", windows W.focusDown)    -- Move focus to the next window
  , ("M-k", windows W.focusUp)      -- Move focus to the prev window
  , ("M-S-m", windows W.swapMaster) -- Swap the focused window and the master window
  , ("M-S-j", windows W.swapDown)   -- Swap focused window with next window
  , ("M-S-k", windows W.swapUp)     -- Swap focused window with prev window
  , ("M-<Backspace>", promote)      -- Moves focused window to master, others maintain order
  , ("M-S-<Tab>", rotSlavesDown)    -- Rotate all windows except master and keep focus in place
  , ("M-C-<Tab>", rotAllDown)       -- Rotate all the windows in the current stack

-- Layouts
  , ("M-<Tab>", sendMessage NextLayout)           -- Switch to next layout
  , ("M-<Space>", sendMessage (MT.Toggle NBFULL) >> sendMessage ToggleStruts) -- Toggles noborder/full

  -- Workspaces
  , ("M-d", nextWS)        -- Switch to next Workspace
  , ("M-a", prevWS)       -- Switch to prev Workspace
  , ("M-C-d", shiftToNext <+> nextWS)
  , ("M-C-a", shiftToPrev <+> prevWS)
  , ("M-S-a", nextScreen)  -- Switch focus to next monitor
  , ("M-S-d", prevScreen)  -- Switch focus to prev monitor
  --, ("M-o", shiftTo Next nonNSP >> moveTo Next nonNSP)       -- Shifts focused window to next ws
  , ("M-S-o", shiftNextScreen >> nextScreen)       -- Shifts focused window to next Screen
  --, ("M-i", shiftTo Prev nonNSP >> moveTo Prev nonNSP)  -- Shifts focused window to prev ws
  , ("M-o", shiftPrevScreen >> prevScreen)  -- Shifts focused window to prev Screen


-- Multimedia Keys
  , ("<XF86AudioPlay>", spawn (myTerminal ++ "mocp --play"))
  , ("<XF86AudioPrev>", spawn (myTerminal ++ "mocp --previous"))
  , ("<XF86AudioNext>", spawn (myTerminal ++ "mocp --next"))
  , ("<XF86AudioMute>", spawn "pactl set-sink-mute @DEFAULT_SINK@ toggle")
  , ("<XF86AudioLowerVolume>", spawn "pactl set-sink-volume @DEFAULT_SINK@ -5%")
  , ("<XF86AudioRaiseVolume>", spawn "pactl set-sink-volume @DEFAULT_SINK@ +5%")
  , ("<XF86MonBrightnessUp>", spawn "xbacklight -inc 5")
  , ("<XF86MonBrightnessDown>", spawn "xbacklight -dec 5")
  , ("<XF86HomePage>", spawn "qutebrowser https://www.youtube.com/c/DistroTube")
  , ("<XF86Search>", spawn "dmsearch")
  , ("<XF86Mail>", runOrRaise "thunderbird" (resource =? "thunderbird"))
  , ("<XF86Calculator>", runOrRaise "qalculate-gtk" (resource =? "qalculate-gtk"))
  , ("<XF86Eject>", spawn "toggleeject")
  , ("<Print>", spawn "dmscrot")
  ]
  ++
  -- Shift and move to workspace
  map (\(w, i) -> ("M-C-"++show i, windows $ shiftAndMove w)) (zipWith (,) myWorkspaces [1..])


-- Layouts
--Makes setting the spacingRaw simpler to write. The spacingRaw module adds a configurable amount of space around windows.
mySpacing :: Integer -> l a -> XMonad.Layout.LayoutModifier.ModifiedLayout Spacing l a
mySpacing i = spacingRaw False (Border i i i i) True (Border i i i i) True


floats   = renamed [Replace "floats"]
           $ smartBorders
           $ limitWindows 20 simplestFloat
tall     = renamed [Replace "tall"]
           $ smartBorders
           $ addTabs shrinkText myTabTheme
           $ subLayout [] (smartBorders Simplest)
           $ limitWindows 12
           $ mySpacing 0
           $ ResizableTall 1 (3/100) (1/2) []
grid     = renamed [Replace "grid"]
           $ smartBorders
           $ addTabs shrinkText myTabTheme
           $ subLayout [] (smartBorders Simplest)
           $ limitWindows 12
           $ mySpacing 0
           $ mkToggle (single MIRROR)
           $ Grid (16/10)
spirals  = renamed [Replace "spirals"]
           $ smartBorders
           $ addTabs shrinkText myTabTheme
           $ subLayout [] (smartBorders Simplest)
           $ mySpacing 8
           $ spiral (6/7)

-- setting colors for tabs layout and tabs sublayout.
myTabTheme = def { fontName            = myFont
                 , activeColor         = myActiveBgColor
                 , inactiveColor       = myInactiveBgColor
                 , activeBorderColor   = myActiveBgColor
                 , inactiveBorderColor = myBgColor
                 , activeTextColor     = myBgColor
                 , inactiveTextColor   = myTextColor
                 }

myLayoutHook = avoidStruts $ mouseResize $ windowArrange $ T.toggleLayouts floats
               $ mkToggle (NBFULL ?? NOBORDERS ?? EOT) myDefaultLayout
             where
               myDefaultLayout =     withBorder myBorderWidth tall
                                ||| grid
                                ||| spirals

-- Hooks
myManageHook :: XMonad.Query (Data.Monoid.Endo WindowSet)
myManageHook = composeAll
     -- 'doFloat' forces a window to float.  Useful for dialog boxes and such.
     -- using 'doShift ( myWorkspaces !! 7)' sends program to workspace 8!
     -- I'm doing it this way because otherwise I would have to write out the full
     -- name of my workspaces and the names would be very long if using clickable workspaces.
     [ className =? "confirm"         --> doFloat
     , className =? "file_progress"   --> doFloat
     , className =? "dialog"          --> doFloat
     , className =? "download"        --> doFloat
     , className =? "error"           --> doFloat
     , className =? "Gimp"            --> doFloat
     , className =? "notification"    --> doFloat
     , className =? "pinentry-gtk-2"  --> doFloat
     , className =? "splash"          --> doFloat
     , className =? "toolbar"         --> doFloat
     --, title =? "Oracle VM VirtualBox Manager"  --> doFloat
     --, title =? "Mozilla Firefox"     --> doShift ( myWorkspaces !! 1 )
     , className =? "Thunderbird"     --> doShift ( myWorkspaces !! 8 )
     , (className =? "firefox" <&&> resource =? "Dialog") --> doFloat  -- Float Firefox Dialog
     , (className =? "Thunderbird" <&&> resource =? "Dialog") --> doFloat  -- Float Thunderbird Dialog
     , isFullscreen -->  doFullFloat
     ] -- <+> namedScratchpadManageHook myScratchPads


main :: IO ()
main = do
  xmproc0 <- spawnPipe "xmobar -x 0 $HOME/.config/xmobar/xmobarrclaptop.hs"
  xmonad $ ewmh def
    { manageHook         = myManageHook <+> manageDocks
    , handleEventHook    = docksEventHook <+> ewmhDesktopsEventHook
    , modMask            = myModMask
    , terminal           = myTerminal
    , startupHook        = myStartupHook
    , layoutHook         = myLayoutHook
    , borderWidth        = myBorderWidth
    , workspaces         = myWorkspaces
    , normalBorderColor  = myBorderColor
    , focusedBorderColor = myActiveBgColor
    , logHook = dynamicLogWithPP $ xmobarPP 
      { ppOutput = \x -> hPutStrLn xmproc0 x
      , ppCurrent = xmobarColor myActiveBgColor ""
      , ppVisible = xmobarColor myUrgentBgColor "" . clickable
      , ppHidden = xmobarColor myTextColor "" . clickable
      , ppTitle = xmobarColor myTextColor "" . shorten 60
      , ppSep = "<fc=#666666> <fn=1>|</fn> </fc>"
      , ppUrgent = xmobarColor myTextColor myUrgentBgColor . wrap "!" "!"
      , ppExtras = [windowCount]
      , ppOrder = \(ws:l:t:ex) -> [ws,l]++ex++[t]
      }
    } `additionalKeysP` myKeys

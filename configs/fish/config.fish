set fish_greeting ""
#neofetch
#
#Setup Gnome-Keyring for ssh 

if test -n "$DESKTOP_SESSION"
    set -x (gnome-keyring-daemon --start | string split "=")
end

# export environment variables

function setenv; 
  if [ $argv[1] = PATH ]
      # Replace colons and spaces with newlines
      set -gx PATH (echo $argv[2] | tr ': ' \n)
  else
    set -gx $argv
  end
end
source ~/.config/env

### Functions ###

# Functions needed for !! and !$
# Will only work in default (emacs) mode.
# Will NOT work in vi mode.
function __history_previous_command
  switch (commandline -t)
  case "!"
    commandline -t $history[1]; commandline -f repaint
  case "*"
    commandline -i !
  end
end

function __history_previous_command_arguments
  switch (commandline -t)
  case "!"
    commandline -t ""
    commandline -f history-token-search-backward
  case "*"
    commandline -i '$'
  end
end
# The bindings for !! and !$
bind ! __history_previous_command
bind '$' __history_previous_command_arguments

### Aliases ###
alias ..="cd .."
alias ...="cd ../.."

alias unlock="sudo rm /var/lib/pacman/db.lck"

alias mirror="sudo reflector -f 30 -l 30 --number 10 --verbose --save /etc/pacman.d/mirrorlist"
alias mirrord="sudo reflector --latest 50 --number 20 --sort delay --save /etc/pacman.d/mirrorlist"
alias mirrors="sudo reflector --latest 50 --number 20 --sort score --save /etc/pacman.d/mirrorlist"
alias mirrora="sudo reflector --latest 50 --number 20 --sort age --save /etc/pacman.d/mirrorlist"

alias grep="grep --color=auto"
alias egrep="egrap --color=auto"
alias fgrep="fgrep --color=auto"

alias ls="exa -al --color=always --group-directories-first"
alias la="exa -a --color=always --group-directories-first"
alias ll="exa -l --color=always --group-directories-first"
alias lt="exa -aT --color=always --group-directories-first"
alias l.='exa -a --color=always | egrep "^\."'


alias cp="cp -i"
alias df="df -h"
alias free="free -m"

alias psmem="ps auxf | sort -nr -k 4"
alias psmem10="ps auxf | sort -nr -k 4 | head -10"

alias pscpu="ps auxf | sort -nr -k 3"
alias pscpu10="ps auxf | sprt -nr -k 3 | head -10"

alias addup="git add -u"
alias addall="git add ."
alias checkout="git checkout"
alias commit="git commit -m"
alias pull="git pull origin"
alias push="git push origin"
alias gstat="git status"
alias gconflicts="git diff --name-only --diff-filter=U"

alias update-configs="~/.config/scripts/updateConfigs.sh"
alias rem="gio trash"

alias vim="nvim"
alias grepClass="xprop | grep CLASS"
alias unmountDisk="udisksctl unmount -b"
alias removeDisk="udisksctl power-off -b"

alias update="$HOME/.config/scripts/updateScript.sh"
alias stopdocker="docker stop (docker ps -a -q)"
alias remdocker="docker rm (docker ps -a -q)"

alias setupBarrierKeyboard="$HOME/.config/scripts/setupBarrierKeyboard.sh"
alias cmatrix="unimatrix -l aAegGnorRS"

# Change to ask on override with cp
alias cp="cp -i"

starship init fish | source

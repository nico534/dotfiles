-- See `:help vim.keymap.set()`
local keymap = vim.keymap.set
-- Silent keymap option
local opts = { silent = true }

-- [[ Basic Keymaps ]]
-- Set <space> as the leader key
-- See `:help mapleader`
--  NOTE: Must happen before plugins are required (otherwise wrong leader will be used)
vim.g.mapleader = ' '
vim.g.maplocalleader = ' '

-- Keymaps for better default experience
keymap({ 'n', 'v' }, '<Space>', '<Nop>', { silent = true })

-- Remap for dealing with word wrap
keymap('n', 'k', "v:count == 0 ? 'gk' : 'k'", { expr = true, silent = true })
keymap('n', 'j', "v:count == 0 ? 'gj' : 'j'", { expr = true, silent = true })


-- Vim-Smooth-Scroll
keymap("n", "<C-u>", ":call smooth_scroll#up(&scroll, 5, 2)<CR>", opts)
keymap("n", "<C-d>", ":call smooth_scroll#down(&scroll, 5, 2)<CR>", opts)

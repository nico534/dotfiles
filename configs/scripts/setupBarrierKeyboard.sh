#!/bin/bash

VIRT_KEYBOARD=$(xinput list | grep "Virtual core XTEST keyboard" | sed -e 's/.\+=\([0-9]\+\).\+/\1/')

setxkbmap -device $VIRT_KEYBOARD de nodeadkeys

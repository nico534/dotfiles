#!/bin/bash
echo "sudo pacman -Syu"
sudo pacman -Syu

# read -p "Press enter to continue"
echo "yay -Syu"
yay -Syu

# update flatpak
echo "flatpak update"
flatpak update

confirm() {
    # call with a prompt string or use a default
    read -r -p "${1:-Are you sure? [y/N]} " response
    case "$response" in
        [yY][eE][sS]|[yY]) 
            true
            ;;
        *)
            false
            ;;
    esac
}


#confirm "Start v4l2loopback?" && echo "start v4l2loopback" && sudo modprobe v4l2loopback devices=1 max_buffers=2 exclusive_caps=1 card_label="VirualCam #0"

#confirm "Start Docker? [Y/N]" && sudo systemctl start docker.service

echo "update config from Git"

# Update Configs
$HOME/.config/scripts/updateConfigs.sh

# Recompile xmonad
xmonad --recompile
xmonad --restart

read -p "Press enter to continue"
exit 0

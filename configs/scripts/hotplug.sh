#!/bin/sh
# Script to automatically plug and unplug monitor.

export XAUTHORITY=/home/pc/.Xauthority

case "$SRANDRD_ACTION" in
  "HDMI-1 connected") 
    xrandr --output HDMI-0 --auto --mode 1920x1080 --panning 1920x1080+0+0 --output HDMI-1 --right-of HDMI-0 --auto
    kill $(pgrep trayer) && trayer --edge top --align right --widthtype request --padding 6 --SetDockType true --SetPartialStrut true --expand true --monitor 1 --transparent true --alpha 0 --tint 0x282c34  --height 22 &
    ;;
  "HDMI-1 disconnected") 
    xrandr --output HDMI-0 --auto --mode 1920x1080 --fb 1920x1080
    kill $(pgrep trayer) && trayer --edge top --align right --widthtype request --padding 6 --SetDockType true --SetPartialStrut true --expand true --monitor 1 --transparent true --alpha 0 --tint 0x282c34  --height 22 &
    ;;
esac

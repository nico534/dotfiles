#!/bin/bash

#TABLET_MOUS=$(xinput list | grep 'WALTOP' | grep 'Mouse' | awk  )
#TABLET_PEN=$(xinput list | grep 'WALTOP' | grep 'Pen')

#echo $TABLET_MOUS
#echo $TABLET_PEN

SEARCH="WALTOP"
#MONITOR="DVI-D-0"
MONITOR="HDMI-0"

if [ "$SEARCH" = "" ]; then 
    exit 1
fi

ids=$(xinput --list | grep 'Mouse\|Pen\|stylus' | awk -v search="$SEARCH" \
    '$0 ~ search {match($0, /id=[0-9]+/);\
                  if (RSTART) \
                    print substr($0, RSTART+3, RLENGTH-3)\
                 }'\
     )

for i in $ids
do
    xinput map-to-output $i $MONITOR
    echo "xinput map-to-output $i $MONITOR"
    #xinput set-prop $i 'Device Accel Profile' -1
    #xinput set-prop $i 'Device Accel Constant Deceleration' 2.5
    #xinput set-prop $i 'Device Accel Velocity Scaling' 1.0
done


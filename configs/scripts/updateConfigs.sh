#!/bin/bash 
cd ~/.dotfiles || exit
BRANCH_NAME=$(git branch --show-current)
if [ "$BRANCH_NAME" == "master" ]
then
  echo "On Master-Branch, Can't update configs"
else
  echo "Update Configs"
  git add .
  git commit -m "Automated-Commit"
  git checkout master
  git pull origin
  git checkout system-branch
  git merge master
fi


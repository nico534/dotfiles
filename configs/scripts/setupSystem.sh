#!/bin/bash

# Set Firefox as default Browser
xdg-settings set default-web-browser firefox.desktop

# install vim-plug
sh -c 'curl -fLo "${XDG_DATA_HOME:-$HOME/.local/share}"/nvim/site/autoload/plug.vim --create-dirs \
       https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'

# install starship shell-prompt
sh -c "$(curl -fsSL https://starship.rs/install.sh)"


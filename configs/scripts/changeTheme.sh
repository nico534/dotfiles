#!/bin/bash

L_GTK_THEME="Adapta-Maia"
D_GTK_THEME="Adapta-Nokto-Maia"
L_QT_THEME="KvAdaptaMaia"
D_QT_THEME="KvAdaptaMaiaDark"
L_IJ_THEME="IntelliJ Light"
D_IJ_THEME="Darcula"

GTK_THEME=""
QT_THEME=""
IJ_THEME=""


LIGHT_THEME=$1
#change GTK-Theme
if [ $LIGHT_THEME -eq 1 ]
then
  echo "Light"
  pgrep redshift && kill redshift
  GTK_THEME=$L_GTK_THEME
  QT_THEME=$L_QT_THEME
  IJ_THEME=$L_IJ_THEME
else
  echo "Dark"
  pgrep redshift || redshift -l 50.65205:9.16344 -t 3500:3000 &
  GTK_THEME=$D_GTK_THEME
  QT_THEME=$D_QT_THEME
  IJ_THEME=$D_IJ_THEME
fi
echo "Set GTK-Theme"
# set GTK-Theme
sed -i "s/^gtk-theme-name.*/gtk-theme-name=$GTK_THEME/" ~/.config/gtk-3.0/settings.ini
sed -i "s/^gtk-theme-name.*/gtk-theme-name=\x22$GTK_THEME\x22/" ~/.gtkrc-2.0

echo "Set QT-Theme"
# set qt/Kvantum theme
sed -i "s/^theme=.*/theme=$QT_THEME/" ~/.config/Kvantum/kvantum.kvconfig

echo "Set IJ-Theme"
# set Intellij-Theme
for dir in ~/.config/JetBrains/*
do
  dir=${dir%*/}
  foundFile=${dir##*/}
  if [ $LIGHT_THEME -eq 1 ]
  then
    sed -i "3s|^.*|    \x3Claf class-name=\x22com.intellij.ide.ui.laf.IntelliJLaf\x22 themeId=\x22JetBrainsLightTheme\x22 /\x3E|" ~/.config/JetBrains/$foundFile/options/laf.xml
  else
    sed -i "3s|^.*|    \x3Claf class-name=\x22com.intellij.ide.ui.laf.darcula.DarculaLaf\x22 /\x3E|" ~/.config/JetBrains/$foundFile/options/laf.xml
  fi
  sed -i "s|^    \x3Cglobal_color_scheme name=.*|    \x3Cglobal_color_scheme name=\x22$IJ_THEME\x22 /\x3E|" ~/.config/JetBrains/$foundFile/options/colors.scheme.xml
done

echo "Set Alacritty-Theme"
# Set alacritty colors
AL_CONFIG_LOCATION=~/.config/alacritty/alacritty.yml
AL_PRIMARY_LINE_NR=$(fgrep -n "  primary:" $AL_CONFIG_LOCATION | head -n 1 | cut -d: -f1)
AL_SELECTION_LINE_NR=$(fgrep -n "  selection:" $AL_CONFIG_LOCATION | head -n 1 | cut -d: -f1)
AL_PRIM_BG=`expr $AL_PRIMARY_LINE_NR + 1`
AL_PRIM_FG=`expr $AL_PRIM_BG + 1`
AL_SEL_BG=`expr $AL_SELECTION_LINE_NR + 1`
AL_SEL_FG=`expr $AL_SEL_BG + 1`

echo $AL_PRIM_BG
echo $AL_PRIM_FG
echo $AL_SEL_BG
echo $AL_SEL_FG

if [ $LIGHT_THEME -eq 1 ]
then
  sed -i "${AL_PRIM_BG}s|^.*|    background: '0xffffff'|" $AL_CONFIG_LOCATION
  sed -i "${AL_PRIM_FG}s|^.*|    foreground: '0x222d32'|" $AL_CONFIG_LOCATION
  sed -i "${AL_SEL_BG}s|^.*|    background: '0xede1e1'|" $AL_CONFIG_LOCATION
  sed -i "${AL_SEL_FG}s|^.*|    text: '0x222d32'|" $AL_CONFIG_LOCATION
else
  sed -i "${AL_PRIM_BG}s|^.*|    background: '0x222d32'|" $AL_CONFIG_LOCATION
  sed -i "${AL_PRIM_FG}s|^.*|    foreground: '0xbbc2cf'|" $AL_CONFIG_LOCATION
  sed -i "${AL_SEL_BG}s|^.*|    background: '0x2f434c'|" $AL_CONFIG_LOCATION
  sed -i "${AL_SEL_FG}s|^.*|    text: '0xbbc2cf'|" $AL_CONFIG_LOCATION
fi



